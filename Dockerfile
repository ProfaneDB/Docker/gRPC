FROM alpine:3.6
LABEL maintainer giorgio.azzinnaro+profanedb@gmail.com

ARG GRPC_TAG=v1.7.x
ENV GRPC_TAG $GRPC_TAG

RUN apk add --no-cache git curl coreutils pkgconfig libstdc++ g++ autoconf automake make libtool openssl-dev file && \
    git clone --recurse-submodules -b $GRPC_TAG https://github.com/grpc/grpc /tmp/grpc && \
    cd /tmp/grpc/third_party/protobuf && ./autogen.sh && ./configure && make -j $(nproc --all) && make install && \
    cd /tmp/grpc && make -j $(nproc --all) && make install && cd / && \
    rm -rf /tmp/grpc

# CMD ["protoc"]
