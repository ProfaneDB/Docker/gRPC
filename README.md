# gRPC Docker image

This image is built on top of [Alpine](https://hub.docker.com/_/alpine/),
and has the most basic build for gRPC with C++.


## Building custom image

`$GRPC_TAG` is used to select the branch (unintuitively... I admit),
which will be checked out from https://github.com/grpc/grpc

## GitLab CI automated build

A GitLab CI script is defined, it compiles gRPC according to [the official guide](https://github.com/grpc/grpc/blob/master/INSTALL.md).

It then uploads the image to the [container registry](https://gitlab.com/ProfaneDB/Docker/gRPC/container_registry) on GitLab.
The image is named `gitlab.registry.com/profanedb/docker/grpc` and tagged both as `latest` and with the branch coming from https://grpc.io/release. 
